package com.gmail.raynlegends.Spammer;

import javax.swing.UIManager;

public class Main {

	private static GUI gui;
	private static Spammer spammer;

	public static void main(String[] args) throws Exception {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			//
		}

		gui = new GUI();
		spammer = new Spammer();
	}

	public static GUI getGUI() {
		return gui;
	}

	public static Spammer getSpammer() {
		return spammer;
	}
}