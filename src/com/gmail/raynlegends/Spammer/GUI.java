package com.gmail.raynlegends.Spammer;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Cursor;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;

public class GUI {

	private JFrame frame;
	private JTextField text;
	private JButton spam;
	private JLabel label;
	private JRadioButton normale;
	private JRadioButton peggio;
	private JRadioButton underground;
	private ButtonGroup buttonGroup = new ButtonGroup();
	private JProgressBar progressBar;
	
	public GUI() {
		frame = new JFrame("Incazzer");
		frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		frame.setSize(new Dimension(500, 200));
		frame.setPreferredSize(new Dimension(500, 200));
		frame.setMinimumSize(new Dimension(500, 200));
		frame.getContentPane().setPreferredSize(new Dimension(500, 200));
		frame.getContentPane().setSize(new Dimension(500, 200));
		frame.getContentPane().setMinimumSize(new Dimension(500, 200));
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		text = new JTextField();
		text.setBounds(0, 11, 494, 89);
		text.setText("Testo da spammare");
		frame.getContentPane().add(text);
		text.setColumns(10);
		
		spam = new JButton("SPAM");
		spam.setFocusable(false);
		spam.setBounds(0, 148, 494, 23);
		spam.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg) {
				Main.getSpammer().start();
			}
		});
		frame.getContentPane().add(spam);
		
		label = new JLabel("Better than underground99 \u1F60");
		label.setFocusable(false);
		label.setBounds(335, 105, 149, 23);
		frame.getContentPane().add(label);
		
		progressBar = new JProgressBar();
		progressBar.setBounds(0, 130, 494, 14);
		frame.getContentPane().add(progressBar);
		
		normale = new JRadioButton("Normale");
		normale.setSelected(true);
		normale.setBounds(10, 102, 65, 26);
		peggio = new JRadioButton("Peggio");
		peggio.setBounds(77, 102, 65, 26);
		underground = new JRadioButton("Underground");
		underground.setBounds(140, 102, 100, 26);
		
		buttonGroup.add(normale);
		buttonGroup.add(peggio);
		buttonGroup.add(underground);
		
		frame.getContentPane().add(normale);
		frame.getContentPane().add(peggio);
		frame.getContentPane().add(underground);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public Type getType() {
		if (normale.isSelected()) {
			return Type.NORMAL;
		} else if (peggio.isSelected()) {
			return Type.WORSE;
		} else if (underground.isSelected()) {
			return Type.UNDERGROUND;
		}
		return null;
	}
	
	public void setEnabled(boolean enabled) {
		normale.setEnabled(enabled);
		peggio.setEnabled(enabled);
		underground.setEnabled(enabled);
		spam.setEnabled(enabled);
	}
	
	public String getText() {
		return text.getText();
	}
	
	public void setButton(String button) {
		this.spam.setText(button);
	}
	
	public void setBar(Integer value) {
		this.progressBar.setValue(value);
	}
}
